package main

import (
	"errors"
	"fmt"
)

type ElemType interface{}

type Node struct {
	Data ElemType
	Next *Node
}

type List struct {
	Head   *Node
	Length int
}

// NewList Содать новый список
func NewList() *List {
	return &List{new(Node), 0}
}

// Add Добавить элемент в конец
func (list *List) Add(number ElemType) {
	if list.Length == 0 {
		list.Head.Data = number
		list.Length++
		return
	}
	copyList := list.Head
	for copyList.Next != nil {
		copyList = copyList.Next
	}
	copyList.Next = &Node{number, nil}
	list.Length++
}

// Print вывод списка
func (list *List) Print() {
	fmt.Print("[")
	copyList := list.Head
	for copyList != nil {
		fmt.Print(copyList.Data)
		if copyList.Next != nil {
			fmt.Print(", ")
		}
		copyList = copyList.Next
	}
	fmt.Print("]")
}

// Pop удалить первый элемент
func (list *List) Pop() {
	list.Head = list.Head.Next
	list.Length--
}

// Delete удалить элемент по индексу
func (list *List) Delete(index int) error {
	if index < 0 || index >= list.Length {
		return errors.New("index not in list")
	}
	if index == 0 {
		list.Pop()
		return errors.New("not error")
	}
	copyList := list.Head
	k := 0
	for k != index-1 {
		copyList = copyList.Next
		k++
	}
	copyList.Next = copyList.Next.Next
	list.Length--
	return errors.New("not error")
}

// Insert вставка элемента по индеку
func (list *List) Insert(index int, number ElemType) error {
	if index < 0 || index >= list.Length {
		return errors.New("index not in list")
	}
	if index == 0 {
		list.Push(number)
		return errors.New("not error")
	}
	copyList := list.Head
	k := 0
	for k != index-1 {
		copyList = copyList.Next
		k++
	}
	copyNext := copyList.Next
	copyList.Next = &Node{number, copyNext}
	list.Length++
	return errors.New("not error")
}

// Push добавить элемент в начало
func (list *List) Push(number ElemType) {
	list.Head = &Node{number, list.Head}
	list.Length++
}

// GetElem получить элемент по индексу
func (list *List) GetElem(i int) (ElemType, error) {
	if i < 0 || i >= list.Length {
		return nil, errors.New("index not in list")
	}
	copyList, k := list.Head, 0
	for k != i {
		copyList = copyList.Next
		k++
	}
	return copyList.Data, errors.New("not error")
}

func main() {
	var lst = NewList()
	fmt.Print("Create list:\n")
	lst.Print()
	fmt.Printf("\nLength:%d\n", lst.Length)

	fmt.Print("\nAdd test:\n")
	lst.Add(1)
	lst.Add("ff")
	lst.Add(3.4)
	lst.Add(4)
	lst.Print()
	fmt.Printf("\nLength:%d\n", lst.Length)

	fmt.Print("\nPop test:\n")
	lst.Pop()
	lst.Print()
	fmt.Printf("\nLength:%d\n", lst.Length)

	fmt.Print("\nDelete true test:\n")
	err := lst.Delete(1)
	fmt.Printf("Delete 1 element: %s\n", err)
	lst.Print()
	fmt.Printf("\nLength:%d\n", lst.Length)

	fmt.Print("\nDelete error test:\n")
	err2 := lst.Delete(-1)
	fmt.Printf("Delete -1 element: %s\n", err2)
	lst.Print()
	fmt.Printf("\nLength:%d\n", lst.Length)

	fmt.Print("\nGetElem true test:\n")
	num, err3 := lst.GetElem(1)
	fmt.Printf("Get 1 element: %d (%s)\n", num, err3)

	fmt.Print("\nGetElem error test:\n")
	num1, err4 := lst.GetElem(10)
	fmt.Printf("Get 10 element: %d (%s)\n", num1, err4)

	fmt.Print("\nInsert true test:\n")
	err5 := lst.Insert(1, 100)
	fmt.Printf("Insert 100 on 1 position: %s\n", err5)
	lst.Print()
	fmt.Printf("\nLength:%d\n", lst.Length)

	fmt.Print("\nInsert error test:\n")
	err6 := lst.Insert(10, 100)
	fmt.Printf("Insert 100 on 10 position: %s\n", err6)
	lst.Print()
	fmt.Printf("\nLength:%d\n", lst.Length)

	fmt.Print("\nPush test:\n")
	lst.Push(10)
	lst.Print()
	fmt.Printf("\nLength:%d\n", lst.Length)
}
